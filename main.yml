---
# Here the magic begins 

# Work on common things for all servers
# En seta sección se defininen elementos en común
# para todos los servidores ejemplo:
#  - Usuarios y grupos
#  - Programas y/o aplicaciones del Sistema Operativo
#  - Time zone y sincronización de hora
#  - Docker y docker-compose
- hosts: all
  become: true
  vars_files:
    - defaults/main.yml
    - defaults/os_vars.yml
  roles:
    # 1. Configure and Install common stuff like users and packages
    - role: 'os/common'
      tags: common
    - role: 'os/shorewall'
      tags: shorewall 
    # 2. Install and set time sync
    - role: 'os/chrony'
      # time_zone: 'America/La_Paz'
      # chrony_config_server:
      #   - 0.pool.ntp.org
      #   - 2.pool.ntp.org
      tags: chrony

    # 3. Install Docker
    - role: docker
      docker_users: ["{{ ansible_user }}", "{{ admin_users}}"]
      tags: docker

    # 4. Install Docker-compose
    - role: docker-compose
      tags: docker_compose


# Acá comienza la instalación y configuración que caracterizan
# a cada servidor cómo por ejemplo:
#   - Check_MK server y client (instalación y configuración inicial)
#   - Centralización de logs  (syslogs y logrotate)
#   - Nexus en el servidor de gestión
#
# Primero se instala el servidor central o adminserver,
# luego el resto de servidores clientes.

# Admin server section
- hosts: adminserver
  become: true
  vars_files:
    - defaults/main.yml
    - defaults/check_mk_vars.yml
    - defaults/nexus_vars.yml
  roles:
    # 1. Install check_mk server
    - role: docker-compose/check_mk
      check_mk_server: yes
      tags: check_mk_server

    # 2. Configure check_mk to monitor all hosts
    - role: docker-compose/check_mk
      add_hosts_to_monitor: yes
      tags: add_hosts_to_monitor

    # 3. Install logs audit
    - role: 'os/oosec'
      oosec_server: yes
      tags: oosec

    # 4. Install rsyslog server
    - role: 'os/syslog'
      syslog_server: yes
      syslog_port: 514
        # - '514/tcp'
      tags: [ 'syslog_server' , 'syslog'] 

    # 5. Install Nexus (docker registry)
    - role: docker-compose/nexus3   
      tags: nexus

    # 6. Install check_mk client
    - role: docker-compose/check_mk
      check_mk_agent: yes
      # check_mk_server_version: "1.6.0p12"
      # check_mk_HOSTNAME: 'vadm.viridian.cc'
      # check_mk_server_port: '8080' 
      check_mk_plugins:
        - mk_docker.py
      tags: check_mk_client 

# Install and configure logrotate syslog on all servers
- hosts: all
  become: yes
  vars_files:
    - defaults/main.yml
  tasks:
    - name: Configure logrotate syslog file
      import_tasks: 'roles/os/logrotate/tasks/syslog_logrotate_file.yml'
      vars:
        daily: true
        compress: true
        rotate: 7
        logrotate:
          include: /etc/logrotate.d
  tags: [ 'logrotate', 'syslog' ]

# Configure logrotate on main server
- hosts: adminserver
  become: true
  vars_files:
    - defaults/main.yml
  tasks:
    - name: Install and configure logrotate server
      include_role:
        name: os/logrotate
      vars:
        servername: "{{ item }}"
        logrotate:
          daily: true
          compress: true
          create: true
          dateext: false
          include: /etc/logrotate.d
          rotate: 7
          shred: true
        logrotate_d:
          - name: "{{ servername }}"
            files:
              - "/var/log/remote/{{ servername }}/auth/*.log"
              - "/var/log/remote/{{ servername }}/msg/*.log"
            options:
              missingok: true
              daily: true
              compress: true
              rotate: 20
              notifempty: true
      # with_items: "{{ query('inventory_hostnames', 'all:!adminserver') }}"
      with_items: 
        - viridian01
        - viridian02
        - vweb01
        - vweb02
  tags: logrotate

# Host clients section
- hosts: all_but_not_adminserver
  become: true
  vars_files:
    - defaults/main.yml
    - defaults/check_mk_vars.yml
  roles:
    # 1. Configure rsyslog client
    - role: os/syslog
      syslog_server: no
      syslog_port: 514
      tags: [ 'syslog_client' , 'syslog' ]

    # 2. Install check_mk client
    - role: docker-compose/check_mk
      check_mk_agent: yes
      check_mk_plugins:
        - mk_docker.py
      tags: check_mk_client    



# Ahora trabajamos sobre las aplicaciones, ejemplo:
#   - Vault
#   - Gitea
#   - vdb Viridian applicaciones
#   - proxy server

# Work on  applications server
- hosts: viridianservers
  become: true
  vars_files:
    - defaults/main.yml
    - defaults/viridian_vars.yml
    - defaults/docker_images_list.yml
    - defaults/check_mk_vars.yml
    - defaults/nginx_vars.yml
  tasks:
    - name: Deploy certificates
      include: roles/docker-compose/vdb/tasks/certificates.yml
      tags: [ 'certificates' , 'tls' ]
    - name: Deploy gitea
      include: roles/docker-compose/vdb/tasks/gitea.yml
      tags: gitea
    - name: Deploy vault
      include: roles/docker-compose/vdb/tasks/vault.yml
      tags: vault
    - name: Deploy kafka
      include: roles/docker-compose/vdb/tasks/kafka.yml
      tags: kafka
    - name: Deploy spring
      include: roles/docker-compose/vdb/tasks/spring.yml
      tags: spring
    - name: Deploy vdb
      include: roles/docker-compose/vdb/tasks/vdb.yml
      tags: [ 'vdb' , 'microservicios' , 'microservices' ]
    - name: Deploy database
      include: roles/docker-compose/vdb/tasks/database.yml
      tags: 'database'
    - name: Deploy mssql database
      include: roles/docker-compose/vdb/tasks/mssql.yml
      tags: 'mssql'
    - name: Deploy greenbank
      include: roles/docker-compose/vdb/tasks/greenbank.yml
      tags: 'greenbank' 
    - name: Deploy front-end
      include: roles/docker-compose/vdb/tasks/front-end.yml
      tags: 'frontend'
  roles:
    # 1. Deploy nginx
    - role: docker-compose/nginx
      sites_enabled: vdb_proxy
      tags: [ 'proxy' , 'nginx' ]


# Work on dmzservers servers
- hosts: dmzservers
  become: true
  vars_files:
    - defaults/main.yml
    - defaults/check_mk_vars.yml
    - defaults/nginx_vars.yml
  tasks:
    - name: Deploy certificates
      include: roles/docker-compose/vdb/tasks/certificates.yml
      tags: [ 'certificates' , 'tls' ]
  roles:
    # 1. Deploy nginx
    - role: docker-compose/nginx
      sites_enabled: dmz_proxy
      tags: [ 'dmz_proxy' , 'dmz_nginx' ]




