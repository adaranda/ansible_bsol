#!/bin/bash
ACTION=$1
CONTAINER=$2
found=false

if [ $# -lt 1 ]
then
  echo "Usage : $0 ps, start|stop|restart container_name"
  exit
fi

function execute() {
if [ -z "$CONTAINER" ]; then
  while read line; do
    [[ "${line:0:1}" = '[' ||  "${line:0:1}" = '#' ]] && continue
    if [ ! -z "$line" ]; then
        $ACTION $line
    fi
  done <~/scripts/containers_list.txt
  exit 0
fi

if grep -qw "^$CONTAINER" ~/scripts/containers_list.txt 2>/dev/null
then
    $ACTION $CONTAINER
else
    found=false
    while read line; do
        [[ $found == false && "$line" != "[$CONTAINER]" ]] &&  continue
        [[ $found == true && "${line:0:1}" = '[' ]] && break
        found=true
        [[ ! -z "$line" &&  $line != "[$CONTAINER]" && "${line:0:1}" != "#" ]] && $ACTION $line
    done <~/scripts/containers_list.txt
fi
}


function start() {
  echo "starting "
  eval "docker start $1"
  ps $1
}

function stop() {
  echo "stopping "
  eval "docker stop $1"
  ps $1
}

function restart() {
  stop $1
  start $1
}

function_exists() {
  declare -f -F $1 > /dev/null
  return $?
}

function ps() {
    if [ "${1:0:1}" == "#" ]; then
	return 0
    fi

    if [ "x$1" == "x" ]; then
    echo -e "3 \e[97m$1\e[0m \e[93mUNKNOWN\e[0m - Container ID or Friendly Name Required"
    exit 0
    fi

    if [ "x$(which docker)" == "x" ]; then
    echo -e "3 \e[97m$1\e[0m \e[93mUNKNOWN\e[0m - Missing docker binary"
    exit 0
    fi

    docker info > /dev/null 2>&1
    if [ $? -ne 0 ]; then
    echo -e "3 \e[97m$1\e[0m \e[93mUNKNOWN\e[0m - Unable to talk to the docker daemon"
    exit 0
    fi

    RUNNING=$(docker inspect --format="{{.State.Running}}" $1 2> /dev/null)

    if [ $? -eq 1 ]; then
    echo -e "3 \e[97m$1\e[0m \e[93mUNKNOWN\e[0m - does not exist."
    return 0
    fi

    if [ "$RUNNING" == "false" ]; then
    echo -e "2 \e[97m$1\e[0m \e[91mCRITICAL\e[0m - is not running."
    return 0
    fi

    RESTARTING=$(docker inspect --format="{{.State.Restarting}}" $1)

    if [ "$RESTARTING" == "true" ]; then
    echo -e "1 \e[97m$1\e[0m \e[93mWARNING\e[0m - state is restarting."
    return 0
    fi

    STARTED=$(docker inspect --format="{{.State.StartedAt}}" $1)
    NETWORK=$(docker inspect --format="{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}" $1)
    HEALTH=$(docker inspect --format="{{.State.Health.Status}}" $1 2> /dev/null)
    [[ -z $HEALTH  ]] && HEALTH="."
    TIME1=$(date +%s)
    TIME2=$(date --date=$STARTED +%s)
    UPS=$(( TIME1 - TIME2 ))
    UPM=$(( UPS/60 ))
    UPH=$(( UPS/3600 ))
    UPD=$(( UPS/3600/24))
    [[ $UPD -gt 0 ]] && UPTIME="$UPD days" || [[ $UPH -gt 0 ]] && UPTIME="$UPH hours"|| UPTIME="$UPM minutes"
    
    echo -e "0 \e[36m$1\e[0m \e[92mOK\e[0m \e[97m$HEALTH\e[0m \e[33m$UPTIME ago\e[0m"
}

case "$ACTION" in
  ps)  function_exists ps && execute
	  ;;
  start)  function_exists start && execute
          ;;
  stop)  function_exists stop && execute
          ;;
  restart)  function_exists restart && execute
          ;;
  *)      echo "Invalid command - Valid->$0 "
	  echo "  ps [container_name|container_group]"
	  echo "  start|stop|restart  container_name|container_group"
          ;;
esac